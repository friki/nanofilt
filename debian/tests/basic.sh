#!/usr/bin/env bash

# Inspired on nanofilt/scripts/test.sh

set -e

# Simple help information test
NanoFilt -h | grep -c nucleotides > /dev/null

# Get test data
git clone --quiet https://github.com/wdecoster/nanotest.git

gunzip -c nanotest/reads.fastq.gz | NanoFilt -q 8 -l 500 --headcrop 50 > /dev/null

# MD5 got experimentally. Probably the correct one
MD5EXPECTED="ca97497c503fa4e1c78b2e9ae126266a"
MD5=$(gunzip -c nanotest/reads.fastq.gz | \
    NanoFilt -q 7 --maxGC 0.75 --headcrop 75 | md5sum | mawk '{print $1}')
[ "$MD5" == "$MD5EXPECTED" ] || (echo Expected $MD5EXPECTED GOT $MD5 ; false)

gunzip -c nanotest/reads.fastq.gz | NanoFilt -q 6 -s nanotest/sequencing_summary.txt > /dev/null

echo Tests passed
