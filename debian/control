Source: nanofilt
Section: python
Priority: optional
Maintainer: Antoni Villalonga <antoni@friki.cat>
Build-Depends: debhelper-compat (= 13),
               python3,
               dh-python,
               python3-setuptools,
Standards-Version: 4.5.0
Homepage: https://github.com/wdecoster/nanofilt
vcs-Browser: https://salsa.debian.org/med-team/nanofilt
Vcs-Git: https://salsa.debian.org/med-team/nanofilt.git
Rules-Requires-Root: no

Package: python3-nanofilt
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${python3:Depends},
Suggests: samtools,
Description: Filtering and trimming of long read sequencing data
 Filtering on quality and/or read length, and optional trimming after passing
 filters. Reads from stdin, writes to stdout. Optionally reads directly from an
 uncompressed file specified on the command line.
 .
 Intended to be used:
   * directly after fastq extraction
   * prior to mapping
   * in a stream between extraction and mapping
